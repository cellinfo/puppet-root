# root

#### Table of Contents

1. [Overview](#overview)
3. [Setup - The basics of getting started with root](#setup)
    * [What root affects](#what-root-affects)
    * [Beginning with root](#beginning-with-root)
4. [Usage - Configuration options and additional functionality](#usage)
    * [Exec ZSH for some clients](#exec-zsh-for-some-clients)
5. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
    * [Classes](#classes)
    * [Parameters](#parameters)
5. [Limitations - OS compatibility, etc.](#limitations)

## Overview

This "root" module simply define few preferences for the root user.

## Setup

### What root affects

* Root's configuration files (/root/.profile, /root/.bashrc,…).
* Root's mail can be forward to other addresses.

### Beginning with root

To begin using root module with default parameter, declare the class with `include root`.

## Usage

### Exec ZSH for some clients

To autoload ZSH as "default" shell, you can pass a regex to the IP_ZSH argument. Then for every new connection, the .profile file will compare the $SSH_CLIENT variable environnment with this regex :

```
class { 'root':
  ip_zsh => "(10.10.10.1|192.168.0.42)",
}
```

### Other default shell

Bash is the default shell use on Debian, if you prefer an advanced shell or didn't have Bash, you can define the DEFAULT_SHELL_PATH argument :

```
class { 'root':
  default_shell_path => '/bin/zsh',
}
```

Then, every connection from 10.10.10.1 or 192.168.0.42 will automatically have ZSH as default shell.

### Forward mail

Root's mail can be forward to other addresses. You can specify thoses addresses with the ADDRESS_TO_FORWARD argument :

```
class { 'root':
  address_to_forward => 'username@domain.org,firstname.lastname@other-domain.org',
}
```

## Reference

### Classes

* `root` : Main class, automatically load all needed privates classes.

### Parameters

* `profile_path` : Path to the .profile to load personal initialization. Must be a absolut path, defaults to '/root/.profile'.
* `profile_content` : Template file used to generate the previous configuration file. Must be a string ending with **.erb**, defaults to 'root/preferences/profile.erb'.
* `bashrc_path` : Path to the Bash's rcfile. Must be a absolut path, defaults to '/root/.bashrc'.
* `bashrc_content` : Template file used to generate the previous configuration file. Must be a string ending with **.erb**, default to 'root/preferences/bashrc.erb'.
* `forward_path` : Path to the forward file. Must be a absolut path, defaults to '/root/.forward'.
* `forward_content` : Template file used to generate the previous configuration file. Must be a string ending with **.erb**, default to 'root/preferences/forward.erb'.
* `address_to_forward` : List of addresses that must receive root's mails. Must be a string, default to ''.
* `default_shell_path` : Path to a bin file to use as default shell for root. Must be a absolut path, defaults to '/bin/bash'.
* `ip_zsh` : A regex to determine if a ssh client should have ZSH as default shell. Defaults to '127.0.0.1'.

## Limitations

This module was only tested on Debian 7.x and 8.x but should work quite few Debian's fork.
