# == Class: root
#
# Define some root's preferences
#
# === Parameters
#
# Document parameters here.
#
# [*sample_parameter*]
#   Explanation of what this parameter affects and what it defaults to.
#   e.g. "Specify one or more upstream ntp servers as an array."
#
# === Variables
#
# Here you should define a list of variables that this module would require.
#
# [*sample_variable*]
#   Explanation of how this variable affects the funtion of this class and if
#   it has a default. e.g. "The parameter enc_ntp_servers must be set by the
#   External Node Classifier as a comma separated list of hostnames." (Note,
#   global variables should be avoided in favor of class parameters as
#   of Puppet 2.6.)
#
# === Examples
#
#  class { 'root':
#    servers => [ 'pool.ntp.org', 'ntp.local.company.com' ],
#  }
#
# === Authors
#
# Author Name <author@domain.com>
#
# === Copyright
#
# Copyright 2016 Your name here, unless otherwise noted.
#
class root (
	$profile_path       = $root::params::profile_path,
	$profile_content    = $root::params::profile_content,
	$bashrc_path        = $root::params::bashrc_path,
	$bashrc_content     = $root::params::bashrc_content,
	$bashrc_content     = $root::params::bashrc_content,
	$forward_path       = $root::params::forward_path,
	$forward_content    = $root::params::forward_content,
	$address_to_forward = $root::params::address_to_forward,
	$default_shell_path = $root::params::default_shell_path,
	$ip_zsh             = $root::params::ip_zsh,
) inherits root::params {


	validate_re($profile_path, '^/')
	validate_re($profile_content, '.erb$')
	validate_re($bashrc_path, '^/')
	validate_re($bashrc_content, '.erb$')
	validate_re($forward_path, '^/')
	validate_re($forward_content, '.erb$')
	validate_string($address_to_forward)
	validate_re($default_shell_path, '^/')


	include '::root::config'

} # public class : root
