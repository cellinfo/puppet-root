# == Private class: root::config
#
class root::config {

	File {
		owner => root,
		group => root,
		mode  => 600,
	}

	# Preferences files
	file { $root::profile_path:
		ensure  => present,
		content => template($root::profile_content),
	}

	file { $root::bashrc_path:
		ensure  => present,
		content => template($root::bashrc_content),
	}

	# Mail
	file { $root::forward_path:
		ensure  => present,
		content => template($root::forward_content),
	}

	# Shell
	user { 'root':
		shell => "${root::default_shell_path}",
	}


} # Private class: root::config
