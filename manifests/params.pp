# == Private class: root::params
#
class root::params {

	# Preferences files
	$profile_path       = '/root/.profile'
	$profile_content    = 'root/preferences/profile.erb'

	$bashrc_path        = '/root/.bashrc'
	$bashrc_content     = 'root/preferences/bashrc.erb'

	# Mail
	$forward_path       = '/root/.forward'
	$forward_content    = 'root/preferences/forward.erb'
	$address_to_forward = ''

	# Shell
	$default_shell_path = '/bin/bash'
	# For those IP, auto-launch ZSH as default shell
	$ip_zsh             = '127.0.0.1'

} # Private class: root::params
